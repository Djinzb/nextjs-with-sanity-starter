export const landingPageQuery = `
*[_type == "landingpage" ] {
  _id,
  hero,
  "background": logo.asset->url
}
`
