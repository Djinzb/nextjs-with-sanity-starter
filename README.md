# Starter project with NextJs and Sanity (based on the example on their github)

## Steps to get this up and running
- First, [create an account on Sanity](https://sanity.io).
  - After creating an account, install the Sanity cli from npm `npm i -g @sanity/cli`.
- Run `sanity init` in `/content`
  - Use default dataset
  - select `Clean project with no predefined schema`
- Generate API-token for the frontend
  - Got to the [sanity dashboard](https://manage.sanity.io/)
  - Select API and Add new token. Add a new token with Read permission
- Create a file called `.env.local` in the root folder and configure environment variables in the file:
  - `NEXT_PUBLIC_SANITY_PROJECT_ID` should be the `projectId` value from the `sanity.json` file created in step 2.
  - `NEXT_PUBLIC_SANITY_DATASET` should be the `dataset` value from the `sanity.json` file created in step 2 - defaults to `production` if not set.
  - `SANITY_API_TOKEN` should be the API token generated in the previous step.
  - `SANITY_PREVIEW_SECRET` can be any random string (but avoid spaces), like `MY_SECRET` - this is used for [Preview Mode](https://nextjs.org/docs/advanced-features/preview-mode).

  Your `.env.local` file should look like this:

  ```bash
  NEXT_PUBLIC_SANITY_PROJECT_ID=...
  NEXT_PUBLIC_SANITY_DATASET=...
  SANITY_API_TOKEN=...
  SANITY_PREVIEW_SECRET=...
  ```
- Enable Sanity previewing: 
  - Go to https://www.sanity.io/docs/preview-content-on-site and follow the three steps on that page. It should be done inside the studio project generated in Step 2.

  - When you get to the second step about creating a file called `resolveProductionUrl.js`, copy the following instead:

    ```js
    const previewSecret = 'MY_SECRET' // Copy the string you used for SANITY_PREVIEW_SECRET
    const projectUrl = 'http://localhost:3000'

    export default function resolveProductionUrl(document) {
      return `${projectUrl}/api/preview?secret=${previewSecret}&slug=${document.slug.current}`
    }
    ```
- After initializing your Sanity studio project there should be a schemas folder in `/content`.
  - Copy the schema file in `./schemas/schema.js` to `./content/schemas/` to replace the `schema.js` file
- Run `sanity start` in the `/content` folder
- Run the following in the root folder
  ```bash
  npm install
  npm run dev

  # or

  yarn install
  yarn dev
  ```

- It should be up and running on [http://localhost:3000](http://localhost:3000) & your studio should be running on [http://localhost:3333](http://localhost:3333)
- Create a landingpage item in the [sanity studio](http://localhost:3333) and see it in the [frontend](http://localhost:3000)


## Custom attributes to change

1. Change the project name in `package.json`
2. Add the website's description for SEO to `/lib/constants`
3. Add favicons in  `/public/favicon`
4. Configure a webhook to trigger in the [sanity dashboard](https://manage.sanity.io/) under API
