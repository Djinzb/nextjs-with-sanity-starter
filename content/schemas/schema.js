import createSchema from 'part:@sanity/base/schema-creator'
import schemaTypes from 'all:part:@sanity/base/schema-type'

export default createSchema({
  name: 'default',
  types: schemaTypes.concat([
    {
      name: 'landingpage',
      type: 'document',
      title: 'Landingspagina',
      fields: [
        {
          name: 'hero',
          title: 'Hero tekst',
          type: 'string',
        },
        {
          name: 'logo',
          title: 'Logo',
          type: 'image',
        },
      ],
    },
  ]),
})
