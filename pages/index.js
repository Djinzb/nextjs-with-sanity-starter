import Head from "next/head";
import { landingPageQuery } from "../lib/queries";
import { getClient, overlayDrafts } from "../lib/sanity.server";
import Layout from "../components/layout";

export default function Index({ initialData }) {
  const { hero, background } = initialData[0];

  return (
    <>
      <Layout>
        <Head>
          <title>Title</title>
        </Head>
        <div className="flex flex-col min-h-screen min-w-full items-center justify-center" style={{background: `url(${background})`, backgroundSize: 'cover'}}>
          <h1 className="text-8xl text-white">{hero}</h1>
        </div>
      </Layout>
    </>
  );
}

export async function getStaticProps({ preview = false }) {
  const initialData = overlayDrafts(
    await getClient(preview).fetch(landingPageQuery)
  );
  return {
    props: { initialData, preview },
  };
}
